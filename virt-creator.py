#!/usr/bin/python
###################################################
# Kyle Hamilton's 1st Professional Python Program
# This program gives a text interface front end to
# the virt-install command which allows creation
# of KVM virtual machines.
###################################################
import curses
import time
import re
import sys
import paramiko
import getpass
import re

# Function to end curses app, restore original terminal. If exit > 1, exit program
def res_term(scr, exit):
  curses.nocbreak()
  scr.keypad(0)
  curses.echo()
  curses.endwin()

  # Exit program
  if(exit > 1):
    exit()

  return 0





# Function to setup a server based on type passed in. Will return an error code and string with explanation, etc
def setup_server(server_type, conn_param):

  # Get username and server ip from connection parameter
  un,host = re.split('@', conn_param)

  # Scripts to run to setup servers based on name
  scripts = { 'webserver': """
sed s/^SELINUX=enforcing$/SELINUX=permissive/g /etc/sysconfig/selinux -i
if [ `hostname` != '""" + host + """' ]; then hostname '""" + host + """'; fi
yum install httpd php mariadb mariadb-server php-pdo php-mysql -y
if [ ! $(egrep '^user1:' /etc/passwd) ]; then useradd -m user1; fi;
if [ ! $(egrep '^user2:' /etc/passwd) ]; then useradd -m user2; fi;
if [ ! $(egrep '^user3:' /etc/passwd) ]; then useradd -m user3; fi;
systemctl enable httpd
systemctl start httpd
systemctl enable mariadb
systemctl start mariadb
echo "create table test.site_visits ( id int NOT NULL AUTO_INCREMENT PRIMARY KEY, site_name varchar(255), counter int); insert into test.site_visits values ( NULL, 'main', 0 )" > /tmp/site_visits.sql
mysql < /tmp/site_visits.sql
rm /var/www/html/index.html
cp /root/files/index.php /var/www/html/index.php
""" }

  print("Setting up [" + server_type + "] using connection params [" + conn_param  + "]")

  # Get password
  pw    = getpass.getpass(un + "'s password:")

  # Set up ssh connection
  ssh_session = paramiko.SSHClient()

  # Auto add server fingerprint to known_hosts file
  ssh_session.set_missing_host_key_policy(paramiko.AutoAddPolicy())

  # Connect
  ssh_session.connect(host, username=un,password=pw)

  if(server_type in 'webserver'):
    for cmd in scripts[server_type].split("\n"):
      # Run command
      print("Running ["  + cmd  + "]")
      stdin, stdout, stderr = ssh_session.exec_command(cmd)
      print("stdout: " + ' ,'.join(stdout.readlines()))
      print("stderr: " + ' ,'.join(stderr.readlines()))
      ####print("\n")
  else:
   # Fail and report why in return val
   retarr = 'Server type passed in unknown! Did not setup server.'
   return [1, reterr]

  # Ran setup commands successfully, return positive run output
  retarr = [0, '']
  return retarr






# My function to write a status string to the console. First arg is screen to draw on, second is string to pass, third is a number >0 meaning to clear the screen
def wrstat(scr, str, clear):
  # Clear screen if pass num >0
  if(clear > 0):
    scr.clear()
  scr.move(1,1)
  scr.deleteln()
  scr.addstr(1,1,str)
  scr.refresh()
  return 0   







# Function to get a configuration parameter from the user. # cpus, amount of ram, etc. Returns user input
# First param is your screen, second is the message to display, third is a regex for the form the response should be in
def config_param(scr,msg,cregex):
  user_input   = ''
  keypress = 0

  # Loop until correct response is received
  while (1 == 1):
    scr.clear()
    wrstat(scr,msg + ' ' + user_input,0)
    keypress = scr.getch()

    if(keypress == 263):
      # Backspace pressed, remove last input char from user input
      user_input = user_input[:-1]
    elif(keypress == 10):
      # See if matches format of user input desired
      if(cregex.match(user_input)):
        break
      else:
        user_input = ''
        msg = 'INPUT ERR ' + msg
    else:
      # Add key to user input var
      user_input += chr(keypress)

  # Return answer since exited loop because user input matches regex passed in
  return user_input






# Function to create the command to create a server
def create_server():
  
  # Initialize curses screen
  scr          = curses.initscr()
  
  # Instantly react to pressed keys instead of requiring enter
  curses.cbreak()
  
  # Do not print characters pressed since no need to press enter
  curses.noecho()
  
  # Translate characters from multibyte to single
  scr.keypad(1)
  
  # Clear and display initial screen, press c to continue creating machine
  wrstat(scr,"virt-creator TUI by Kyle Hamilton.",1)
  time.sleep(2)
  
  # Get server parameters
  opts['meta'] = { 'svr_name' : config_param(scr,"What is the name for this server? ",re.compile("[\d\w\-]+")) }
  opts['inst_dir'] = config_param(scr,"What is the absolute path for the install files? ",re.compile(".*"))
  opts['svr_arch'] = config_param(scr,"What architecture model? 1 for 32 bit, 2 for 64 bit: ",re.compile("[12]"))
  opts['num_cpus'] = config_param(scr,"How many CPU cores should this server have? Please enter a number: ",re.compile("\d+"))
  opts['megs_ram'] = config_param(scr,"How many megabytes of ram should this server have? Please enter a number: ",re.compile("\d+"))
  opts['diskgigs'] = config_param(scr,"How many gigabytes should the hard drive hold? Please enter a number: ",re.compile("\d+"))
  # Get storage pool, iso location?
  
  # Get meta params
  meta_config = config_param(scr,"Would you like to configure metadata for this server? (description, title, uuid) (y/n):",re.compile("[yn]{1}"))
  if(meta_config == 'y'):
    opts['meta']['svr_desc'] = config_param(scr,"Description: ",re.compile(".*"))
    opts['meta']['svr_title'] = config_param(scr,"Title: ",re.compile(".*"))
    opts['meta']['svr_uuid'] = config_param(scr,"UUID: ",re.compile(".*"))
  
    # Construct metadata portion of command
    metaopts = "--metadata name='" + opts['meta']['svr_name'] + "',title='" + opts['meta']['svr_title'] + "',uuid='" + opts['meta']['svr_uuid'] + "',description='" + opts['meta']['svr_desc'] + "'"
  else:
    opts['meta']['svr_desc'] = ''
    opts['meta']['svr_title'] = ''
    opts['meta']['svr_uuid'] = ''
    metaopts = ''
  
  # Convert arch from 1/2 to actual input
  if(opts['svr_arch'] == 1):
    opts['svr_arch'] = 'x86'
  else:
    opts['svr_arch'] = 'x86_64'
  
  # Construct command
  command = "virt-install --name " + opts['meta']['svr_name'] + " --disk size=" + opts['diskgigs'] + " --location " + opts['inst_dir'] + " --arch " + opts['svr_arch'] + " --vcpus " + opts['num_cpus'] + "  --memory " + opts['megs_ram'] + " " + metaopts
  
  # Display output in terminal of server stats and command to create
  res_term(scr, 0)
  summary = "Hostname: " + opts['meta']['svr_name'] + "\nCPU Cores: " + opts['num_cpus'] + "\nGigs of ram: " + opts['megs_ram'] + "\nCommand: " + command
  print(summary)







######
##### Program end
######

#####
#### Main program start
#####

# Init vars
command      = 'virt-install '
keypress     = -1
opts         = {}
opts['meta'] = { 'svr_title' : '', 'svr_desc' : '', 'svr_uuid' : '' }
metaopts     = ''
args         = sys.argv
usage        = sys.argv[0] + """ <action> [<user>@<ip/hostname>]
Action should be create to use TUI to create vm, or webserver to setup a newly created vm webserver. If setting up a server, include username and ip.
"""

# Correct number of args passed?
if((len(args) <= 1) or (len(args) > 4)):
  # Num args incorrect
  print(usage)
  exit()
else:
  # Decide whether to create a command to create a KVM server, auto-setup a newly created server, or fail out if incorrect arguments are used.
  if(args[1] == 'create'):
    # Create command to create server
    create_server()
  elif(args[1] in 'webserver'):
    # Specified type of server to set up. Pass name to set up and conn params in arg 2. Handle return value array here
    retval = setup_server(args[1],args[2])
    if(retval[0] == 0):
      print("Setup of server successful. \n" + retval[1])
  elif(args[1] != 'create'):
    # Does not, not equal, create (nots cancel out, so specified create. Args do not equal webserver, either. Fail out!
    print(usage)

# Exit successfully
exit()
