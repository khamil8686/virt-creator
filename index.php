<?php
  $dsn = 'mysql:host=localhost;dbname="test"';
  $un  = "root";
  $pw  = "";
  $pdo = new PDO("$dsn", "$un", "$pw");
  $sql = 'select counter from test.site_visits where site_name = "main"';
  $inc_sql = 'update test.site_visits set counter = counter + 1 where site_name = "main"';
  $visits = 0;

  # Get # visits
  $pdo_sth = $pdo->prepare("sql");
  $pdo_sth->execute();
  $results = $pdo_sth->fetch(PDO::FETCH_ASSOC);
  $visits = $results['counter'];

  # Increment visits
  $pdo_sth = $pdo->prepare("$inc_sql");
  $pdo_sth->execute();

  echo "This site has been visited $visits times!";
?>
